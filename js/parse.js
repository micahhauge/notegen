// will parse the midi file into a JSON so that it is game readable
var part = 0;
var audio;
// coresponds to the temp of the song
var iRatio = 385;

// open file
var openFile = function(event) {
  var input = event.target;
  var reader = new FileReader();
  reader.onload = function(e) {
    var parts = MidiConvert.parseParts(e.target.result);
    parse(parts[part]);
    // console.log(parts[part]);
    // alert(parts[part][0].time)
  }

  reader.readAsBinaryString(input.files[0]);

}


// function to parse the midi file into a game readable array of noteObjects
function parse(midiObj) {
  // create an array to hold all of the note objects
  var notes = [];

  // function to create note objects
  function Note(pitch, startTime, len) {
    this.pitch = pitch;
    this.startTime = startTime;
    this.stopTime = startTime;
    this.len = len;
    this.graphic = null;
    if (getRandomBool()) {
      this.noteColor = getRandomColor();
    } else {
      this.noteColor = getRandomColor();
    }
  }


  // loop to add notes from midi file to note array
  for (var i = 0; i < midiObj.length; i++) {
    pitch = midiObj[i].midiNote - 21;
    startTime = parseFloat(midiObj[i].time) / iRatio;
    len = parseFloat(midiObj[i].duration) / iRatio;
    note = new Note(pitch, startTime, len);

    notes.push(note);
  }

  console.log(notes);

  var myJsonString = JSON.stringify(notes);

  saveText( JSON.stringify(notes), "filename.json" );
}


function saveText(text, filename){
  var a = document.createElement('a');
  a.setAttribute('href', 'data:text/plain;charset=utf-u,'+encodeURIComponent(text));
  a.setAttribute('download', filename);
  a.click()
}

function getRandomColor () {
  return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}

function getRandom(min, max) {
  return Math.random() * (max - min + 1) + min;
}

function getRandomBool() {
  return Math.round(Math.random());
}
