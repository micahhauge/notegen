// run when DOM is ready
$(document).ready(function(){

TweenLite.defaultEase = Linear.easeNone;
var i, j, x;

var $body = document.body;

// get screen size
var viewWidth = window.innerWidth;
var viewHeight = window.innerHeight;
var centerScreen = viewWidth / 2;

// The value of all black notes
var blackNotes = [1,4,6,9,11,13,16,18,21,23,25,28,30,33,35,37,40,42,45,47,49,52,54,57,59,61,64,66,69,71,73,76,78,81,83,85];

// array of all possible pitch values and corresponding Xpos values (works like a dict of ints)
var pitchToXPos = [0,1,1,2,3,3,4,4,5,6,6,7,7,8,8,9,10,10,11,11,12,13,13,14,14,15,15,16,17,17,18,18,19,20,20,21,21,22,22,23,24,24,25,25,26,27,27,28,28,29,29,30,31,31,32,32,33,34,34,35,35,36,36,37,38,38,39,39,40,41,41,42,42,43,43,44,45,45,46,46,47,48,48,49,49,50,50,51];


// establish scale of notes and keys based on screen size
var noteScale = viewWidth / 52;

// seconds to cross screen
var noteSpeed = 2.5;

// TEMP !!!
noteAreaHeight = viewHeight;

// ship properties
var shipSpeed = 1.5 * noteSpeed;

// for vertical scaling
var yScale = noteAreaHeight / noteSpeed;
console.log('yScale: ' + yScale);

// create timeline for notes
var tl = new TimelineMax({repeat: 300, paused:true});

// create an array of note graphics objects
var noteGraphics = [];

// function to create a NoteGraphic object
function NoteGraphic () {
  this.graphic = document.createElement('div');
  this.graphic.className = "note";
  addToDom(this.graphic);
}

// loop to fill noteGraphics array with noteGraphics objects
for (i = 0; i < 80; i++) {
  noteGraphics.push(new NoteGraphic());
}

// array to hold note objects
var notes = [];

// function to create note objects
function Note(pitch, startTime, len) {
  this.pitch = pitch;
  this.startTime = startTime;
  this.stopTime = startTime + noteSpeed;
  this.len = len * yScale;
  this.graphic = null;
  if (getRandomBool()) {
    this.noteColor = getRandomColor();
  } else {
    this.noteColor = getRandomColor();
  }
}

function createRandomNotes() {
  var min = .1;
  var max = .3;
  var base = 0;
  var dir;

  // loop to make a few thousand notes for benchmark purposes
  for (i = 1; i < 100; i++) {
    x = i;
    while (x > 27) {
      x -= 27;
    }


    base = getRandom(20, 23);
    dir = getRandomBool();

    for (j = 0; j < 8; j++) {
      if (dir) {
        notes.push(new Note(base + (2 * j), i + (j*.1), .1));
        // notes.push(new Note(base + (2 * j) + 5, i + (j*.5), .3));
        // notes.push(new Note(base + (2 * j) + 9, i + (j*.5), .3));
      } else {
        notes.push(new Note(base - (2 * j), i + (j*.5), .3));
        // notes.push(nee(base + (2 * j) + 5, i + (j*.5), .1));
        // notes.push(new N ote(base + (2 * j) + 10, i + (j*.5), .1));
      }
    }

    // scale chords
    // notes.push(new Note(x, i * 1, getRandom(min, max)));
    // notes.push(new Note(x + 5, i * 1, getRandom(min, max)));
    // notes.push(new Note(x + 8, i * 1,  getRandom(min, max)));
    // notes.push(new Note(x + 15, i * 1, getRandom(min, max)));
    // notes.push(new Note(x + 17, i * 1, getRandom(min, max)));
    // notes.push(new Note(x + 3, i * 1, getRandom(min, max)));


    // chord
    // if (!(i % 5)) {
    //   notes.push(new Note(base + 25, i * .4 + .4, .4));
    //   notes.push(new Note(base + 28, i * .4 + .4, .4));
    //   notes.push(new Note(base + 30, i * .4 + .4, .4));
    // }

    // notes.push(new Note(base + 10, i * .25 + 1, .1));
    // notes.push(new Note(base + 13, i * .25 + 1, .1));
    // notes.push(new Note(base + 18, i * .25 + 1, .1));

    // generic scales
    // notes.push(new Note(x + 25, i * .25, 1));
    // notes.push(new Note(x + 27, i * .25, .1));
    // notes.push(new Note(x + 29, i * .25, .1));
    // notes.push(new Note(x + 31, i * .25, .1));
  }
}
// createRandomNotes();

function loadNotes() {
  console.log("in load notes");
  $.getJSON( "json/filename.json", function( notes ) {
    // loop to assign each noteObject to a noteGraphic object
    for (i = 0; i < notes.length; i++) {

      // reset according to screen dimensions
      notes[i].stopTime += noteSpeed;
      notes[i].len = notes[i].len * yScale;

      notes[i].pitch = pitchToXPos[notes[i].pitch];



      x = i;
      while (x >= noteGraphics.length) {
        x -= noteGraphics.length;
      }
      notes[i].graphic = noteGraphics[x].graphic;
    }

    console.log(notes);
    createTimeline(notes);
  });


}

loadNotes();




function createTimeline(notes) {
  console.log("in createTimeline function");
  print(notes);
  for (i = 0; i < notes.length; i++) {
    // set note position and width
    tl.set(notes[i].graphic, {
      backgroundColor: notes[i].noteColor,
      width: noteScale,
      height: notes[i].len,

      x: notes[i].pitch * noteScale,
      rotation: 0.01
    }, notes[i].startTime);

    // // fancy starting animation
    // tl.fromTo(notes[i].graphic, .3, {
    //   // rotation: 999,
    //   // x: centerScreen ,
    //   x: 0,
    // }, {
    //   x: notes[i].pitch * noteScale,
    //   // rotation: '+=360',
    //   // ease: Back.easeOut.config(1.7),
    // }, notes[i].startTime);

    // make the note visible
    tl.to(notes[i].graphic, 0, {
      visibility: 'visible'
    }, notes[i].startTime);

    // animate note downward
    tl.fromTo(notes[i].graphic, noteSpeed, {
      // y: -100,
      y: 100 - notes[i].len,
      rotation: 0.01,
      z:0.01
    }, {
      y: viewHeight - notes[i].len,
      // y: viewHeight - 50,
      rotation: 0.01,
      // rotation: 1000.01,
      z:0.01
    }, notes[i].startTime);

    // make the note invisible to save resources
    tl.to(notes[i].graphic, 0, {
      visibility: 'hidden'
    }, notes[i].stopTime);

  }

  // play timeline
  console.log("Number of notes: ", notes.length);
  console.log("Duration of song: ", tl.duration(), " seconds");
  console.log("Notes per second: ", notes.length / tl.duration());
  tl.play();
  console.log("playing audio");
  audio = new Audio('pirates.mp3').play();
}

var ship = $("#ship");
// var moveRight = TweenLite.to(ship, 10, {left:5000, paused:true});
// var moveLeft = TweenLite.to(ship, 10, {left:-5000, paused:true});

var lateralMovement = new TimelineMax({repeat: 0, paused:true});
var verticalMovement = new TimelineMax({repeat:0, paused:true});

// vertical Movement timeline (for moving ship vertically)
verticalMovement.fromTo(ship, shipSpeed, {
  y: 0,
}, {
  y: viewHeight- 64,
});

// lateral Movement timeline (for moving ship horizontally)
lateralMovement.fromTo(ship, shipSpeed, {
  x: 0,
}, {
  x: viewWidth - 64,
});



function isOverlap(rect1, rect2) {
  var overlap = !(rect1.right < rect2.left ||
    rect1.left > rect2.right ||
    rect1.bottom < rect2.top ||
    rect1.top > rect2.bottom);
}

alert("hi");

console.log(isOverlap(ship, noteGraphic[i]));

$(window).keydown(function(e) {
    // console.log("keydown");
    // console.log(e.which);

    // if w
    if (e.which == 87) {
      verticalMovement.resume();
      verticalMovement.reverse();
    }

    // if a
    if (e.which == 65) {
      lateralMovement.resume();
      lateralMovement.reverse();
    }

    // if s
    if (e.which == 83) {
      verticalMovement.resume();
      verticalMovement.play();
    }

    // if d
    if (e.which == 68) {
      lateralMovement.resume();
      lateralMovement.play();
    }
});

$(window).keyup(function(e) {
    // console.log("keyup");
    // console.log(e.which);
    // if w
    if (e.which == 87) {
      // verticalMovement.pause();
    }

    // if a
    if (e.which == 65) {
      // lateralMovement.pause();
    }

    // if s
    if (e.which == 83) {
      // verticalMovement.pause();
    }

    // if d
    if (e.which == 68) {
      // lateralMovement.pause();
    }
});


function getRandomColor () {
  return '#'+(Math.random()*0xFFFFFF<<0).toString(16);
}

function getRandom(min, max) {
  return Math.random() * (max - min + 1) + min;
}

function getRandomBool() {
  return Math.round(Math.random());
}


// function to remove note div from dom
function addToDom(graphic) {
  //$('body').append(graphic);
  $body.appendChild(graphic);
}

function removeFromDom(graphic) {
  //document.body.removeChild(graphic);
  TweenLite.set(graphic, {display: "none"});
}

// function to determine if a key is white or blackNote
function isBlack(pitch) {
  // if the note is black
  if (blackNotes.indexOf(pitch) != -1) {
    return true;
  } else {
    return false;
  }
}

function isCollide(a, b) {
  alert($(a).position);
  alert($(b).position);
});
